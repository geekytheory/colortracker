//
//  colortraker.cpp
//
//  Created by Jesualdo Ros Arlanzón on 14/05/13.
//  Copyleft (c) 2013 Geekytheory. All wrongs reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include "opencv2/opencv.hpp"

#define VIDEO   "Video"
#define OBJECT  "Objeto"

IplImage * frame;

CvRect selection;
CvScalar color2track = cvScalar(2550,2550,2550);

IplImage * imgTracking;
int lastX = -1;
int lastY = -1;

void selectObject();
void mouseCallback(int event, int x, int y, int flags, void* param);

// Binariza la imagen
IplImage * imageThreshold( IplImage* image ) {
	IplImage * imgThresh=cvCreateImage(cvGetSize(image), IPL_DEPTH_8U, 1);
	cvInRangeS(image,
               cvScalar(color2track.val[0] - 10, color2track.val[1] - 10, color2track.val[2] - 10),
               cvScalar(color2track.val[0] + 10, color2track.val[1] + 10, color2track.val[2] + 10),
               imgThresh);
	return imgThresh;
}

void selectObject() {
    
    cvSetMouseCallback(VIDEO, mouseCallback);
    printf("Selecciona el arrea con el color que quieres seguir\n");
    printf("Pulsa cualquier tecla para continuar\n");
    cvWaitKey();
    if (selection.width > 0 && selection.height > 0) {
        CvRect oldROI = cvGetImageROI(frame);
        cvSetImageROI(frame, selection);
        color2track = cvAvg(frame);
        cvSetImageROI(frame, oldROI);
    }
    cvSetMouseCallback(VIDEO, NULL);
    
}

void mouseCallback(int event, int x, int y, int flags, void* param) {
    
    switch (event) {
            
        case CV_EVENT_LBUTTONDOWN:
            selection.x = x;
            selection.y = y;
            break;
            
        case CV_EVENT_MOUSEMOVE:
            if (flags == CV_EVENT_FLAG_LBUTTON) {
                selection.width = x - selection.x;
                selection.height = y - selection.y;
            }
            break;
        case CV_EVENT_LBUTTONUP:
            selection.width = x - selection.x;
            selection.height = y - selection.y;
            break;
    }
    
    // Mostrar el area seleccionada con el raton
    IplImage * copy =  cvCloneImage(frame);
    cvRectangleR(copy, selection, CV_RGB(255, 0, 0));
    cvShowImage(VIDEO, copy);
    // Siempre liberar memoria
    cvReleaseImage(&copy);
}

void trackObject(IplImage* imageThresh ) {
	// Calculo de los momentos de la imagen
	CvMoments * moments = (CvMoments * )malloc(sizeof(CvMoments));
	cvMoments(imageThresh, moments, 1);
    
	double moment10 = cvGetSpatialMoment(moments, 1, 0);
	double moment01 = cvGetSpatialMoment(moments, 0, 1);
	double area = cvGetCentralMoment(moments, 0, 0);
    
    // Si el area < 500 podemos considerar que no se trata de un objeto, si no de ruido
	if( area > 500){
		// Calculamos la posicion
		int posX = moment10/area;
		int posY = moment01/area;
        
		if( lastX >= 0 && lastY >= 0 && posX >= 0 && posY >= 0 ) {
			// Dibujar una linea 
			cvLine(imgTracking, cvPoint(posX, posY), cvPoint(lastX, lastY), color2track, 4);
		}
        
		lastX = posX;
		lastY = posY;
	}
    
	free(moments);
}

int main(int argc, char *argv[] ) {
	
	printf("A partir de un un video o camara el programa traza el recorrido de un objeto segun su color.\n");
	printf("El color del objeto que deseamos seguir se indica seleccionandolo en el video. Para ello congelamos la imagen pulsando la barra espaciadora y seleccionamos el area de la imagen donde se encuentra el color.\n");
    
    const char * aviFile = (argc >= 2) ? argv[1] : "video"; // Podemos capturar desde un AVI
    
	CvCapture * capture = cvCaptureFromCAM(0);
	
    if( !capture ) {
		printf("No se ha podido iniciar la captura de video\n");
        printf("Capturanco el archivo %s\n", aviFile);
        capture = cvCaptureFromAVI(aviFile);
        if( !capture ) {
            printf("No se ha podido iniciar la captura del archivo %s\n", aviFile);
            return -1;
        }
	}
    
    cvSetCaptureProperty(capture, CV_CAP_PROP_FRAME_WIDTH, 640);
	cvSetCaptureProperty(capture, CV_CAP_PROP_FRAME_HEIGHT, 480);
    
	IplImage * capturedFrame = cvQueryFrame(capture);
	if( !capturedFrame ) return -1;
    
    // Crear una imagen blanca del mismo tamano que el tamano del video
	imgTracking = cvCreateImage(cvGetSize(capturedFrame), IPL_DEPTH_8U, 3);
	cvZero(imgTracking); // convertir la imagen a negro
    
	cvNamedWindow(VIDEO);
	cvNamedWindow(OBJECT);
    
	// Recorrer en bucle el video
	for(;;) {
        
		IplImage * capturedFrame = cvQueryFrame(capture);
		if( !capturedFrame ) break;
		frame = cvCloneImage(capturedFrame);
        
        // Suavizamos la imagen original limpiando el ruido con un filtro gaussiaon
		cvSmooth(frame, frame, CV_GAUSSIAN,3,3);
        
        
        IplImage* imgThresh = imageThreshold(frame);
        
        // Suavizamos la imagen binaria limpiando el ruido con un filtro gaussiaon
		cvSmooth(imgThresh, imgThresh, CV_GAUSSIAN, 3 , 3); 
        
		// Seguimos el objeto
		trackObject(imgThresh);
        
		// Sumamos la imagen con la trayectoria a la capturada 
		cvAdd(frame, imgTracking, frame);
        
		cvShowImage(OBJECT, imgThresh);
		cvShowImage(VIDEO, frame);
        
		// Espera 10 ms
		int c = cvWaitKey(10);
		        
        if( (char)c == 32 )
            selectObject();
        
        // Liberar memoria
		// cvReleaseImage(&imgHSV);
		cvReleaseImage(&imgThresh);
		cvReleaseImage(&frame);
        
        // Si se pulsa ESC sale del bucle
		if( (char)c == 27 )
            break;
	}
    
	cvDestroyAllWindows() ;
	cvReleaseImage(&imgTracking);
	cvReleaseCapture(&capture);     
    
	return 0;
}