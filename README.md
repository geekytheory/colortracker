#ColorTracking
Seguimiento de un color con OpenCV. A partir de un un video o camara el programa traza el recorrido de un objeto segun su color. El color del objeto que deseamos seguir se indica seleccionandolo en el video. Para ello congelamos la imagen pulsando la barra espaciadora y seleccionamos el area de la imagen donde se encuentra el color.

####Compilar

	cmake .
	make

####Ejecución

	./colortraker video.avi